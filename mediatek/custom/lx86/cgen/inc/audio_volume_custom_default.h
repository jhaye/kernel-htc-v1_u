#ifndef AUDIO_VOLUME_CUSTOM_DEFAULT_H
#define AUDIO_VOLUME_CUSTOM_DEFAULT_H

#define AUD_VOLUME_RING \
32, 48, 64, 80, 96, 112, 128,     \
32, 48, 64, 80, 96, 112, 128,     \
32, 48, 64, 80, 96, 112, 128

#define AUD_VOLUME_KEY \
112, 136, 160, 184, 208, 232, 255,     \
112, 136, 160, 184, 208, 232, 255,     \
112, 136, 160, 184, 208, 232, 255

#define AUD_VOLUME_MIC \
64, 112, 192, 176, 192, 192, 176,     \
255, 192, 192, 192, 192, 192, 192,     \
255, 208, 192, 196, 192, 192, 196

#define AUD_VOLUME_FMR \
32, 48, 64, 80, 96, 112, 128,     \
72, 78, 85, 93, 101, 119, 130,     \
32, 48, 64, 80, 96, 112, 128

#define AUD_VOLUME_SPH \
76, 88, 104, 120, 128, 136, 144,     \
68, 80, 92, 104, 116, 128, 140,     \
88, 100, 112, 124, 140, 144, 152

#define AUD_VOLUME_SID \
0, 0, 16, 0, 0, 0, 0,     \
0, 0, 32, 0, 0, 0, 0,     \
0, 0, 0, 0, 0, 0, 0

#define AUD_VOLUME_MEDIA \
32, 48, 64, 80, 96, 112, 128,     \
72, 78, 85, 93, 101, 119, 130,     \
32, 48, 64, 80, 96, 112, 128

#define AUD_VOLUME_MATV \
32, 48, 64, 80, 96, 112, 128,     \
72, 78, 85, 93, 101, 119, 130,     \
32, 48, 64, 80, 96, 112, 128

#endif
